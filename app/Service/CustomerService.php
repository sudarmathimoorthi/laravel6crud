<?php

namespace App\Service;
use App\Model\Customer\CustomerTable;
// use App\EventLog\EventLog;
use DB;
use Redirect;

class CustomerService
{
   
    public function saveCustomer($request)
    {
    	$data =  $request->all();
    	DB::beginTransaction();
    	try {
			$model = new CustomerTable();
        	$addagent = $model->saveCustomer($request);
			if($addagent>0){
				DB::commit();
				$msg = 'Customer Added Successfully';
				if(isset($data['sales']) && $data['sales'])
					return $addagent;
				else
					return $msg;
			}
	    }
	    catch (Exception $exc) {
	    	DB::rollBack();
	    	$exc->getMessage();
	    }
	}
	
	//Get All Customer List
	public function displayAllCustomer()
    {
		$model = new CustomerTable();
		// if(session('userType') == 'admin'){
			$result = $model->displayAllCustomer();
		// }
		// else{
		// 	$result = $model->fetchcustomerDetailsBySessionShop();
		// }
        return $result;
	}

	//Get Customer List By Session Shop
	public function getcustomerDetailsBySessionShop($request)
    {
		$model = new CustomerTable();
        $result = $model->fetchcustomerDetailsBySessionShop($request);
        return $result;
	}

	//Get Particular Customer Details
	public function getCustomerById($id)
	{
		$Model = new CustomerTable();
        $result = $Model->fetchCustomerById($id);
        return $result;
	}
	//Update Particular Customer Details
	public function updateCustomer($params,$id)
    {
    	DB::beginTransaction();
    	try {
			$Model = new CustomerTable();
        	$add = $Model->updateCustomer($params,$id);
			if($add>0){
				DB::commit();
				$msg = 'Customer Updated Successfully';
				return $msg;
			}
	    }
	    catch (Exception $exc) {
	    	DB::rollBack();
	    	$exc->getMessage();
	    }
	}
}

?>