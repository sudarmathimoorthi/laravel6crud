<?php

namespace App\Service;
use DB;
use Redirect;

class CommonService
{
    public function dateFormat($date) {
        if (!isset($date) || $date == null || $date == "" || $date == "0000-00-00") {
            return "0000-00-00";
        } else {
            $dateArray = explode('-', $date);
            if (sizeof($dateArray) == 0) {
                return;
            }
            $newDate = $dateArray[2] . "-";

            $monthsArray = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
            $mon = 1;
            $mon += array_search(ucfirst($dateArray[1]), $monthsArray);

            if (strlen($mon) == 1) {
                $mon = "0" . $mon;
            }
            return $newDate .= $mon . "-" . $dateArray[0];
        }
    }

    public function humanDateFormat($date) {
        if ($date == null || $date == "" || $date == "0000-00-00" || $date == "0000-00-00 00:00:00") {
            return "";
        } else {
            $dateArray = explode('-', $date);
            $newDate = $dateArray[2] . "-";

            $monthsArray = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
            $mon = $monthsArray[$dateArray[1] - 1];

            return $newDate .= $mon . "-" . $dateArray[0];
        }
    }

    public function getDateTime($timezone = 'Asia/Calcutta')
    {
        $date = new \DateTime(date('Y-m-d H:i:s'), new \DateTimeZone($timezone));
        return $date->format('Y-m-d H:i:s');
    }

    public function checkNameValidation($request)
    {
        $tableName = $request['tableName'];
        $fieldName = $request['fieldName'];
        $value = trim($request['value']);
        $fnct = $request['fnct'];

        // var_dump($fnct);
        $user = array();
        try {
            if (isset($fnct) && trim($fnct) !== '') {
                $fields = explode("##", $fnct);
                $primaryName = $fields[0];
                $primaryValue = trim($fields[1]);
                if ($value != "") {
                    $user = DB::table($tableName)
                        ->where($primaryName, '!=', $primaryValue)
                        ->where($fieldName, '=', $value)
                        ->get();
                }
            } else {
                if ($value != "") {
                    $user = DB::table($tableName)
                        ->where($fieldName, '=', $value)
                        ->get();
                }
            }
        } catch (Exception $exc) {
            error_log($exc->getMessage());
        }
        return count($user);
    }

  
}
?>