<?php

namespace App\Service;
use App\Model\Employee\EmployeeTable;
// use App\EventLog\EventLog;
use DB;
use Redirect;

class EmployeeService
{
   
    public function saveEmployee($request)
    {
    	$data =  $request->all();
    	DB::beginTransaction();
    	try {
			$empModel = new EmployeeTable();
        	$addemp = $empModel->saveEmployee($request);
			if($addemp>0){
				DB::commit();
				$msg = 'Employee Added Successfully';
				return $msg;
			}
	    }
	    catch (Exception $exc) {
	    	DB::rollBack();
	    	$exc->getMessage();
	    }
	}
	
	//Get All Employee List
	public function displayAllEmployee($request)
    {
		$empModel = new EmployeeTable();
        $result = $empModel->displayAllEmployee($request);
        return $result;
	}
	//Get Particular Employee Details
	public function getEmployeeById($id)
	{
		$empModel = new EmployeeTable();
        $result = $empModel->fetchEmployeeById($id);
        return $result;
	}
	//Update Particular Employee Details
	public function updateEmployee($params,$id)
    {
    	DB::beginTransaction();
    	try {
			$empModel = new EmployeeTable();
        	$addEmployee = $empModel->updateEmployee($params,$id);
			if($addEmployee>0){
				DB::commit();
				$msg = 'Employee Updated Successfully';
				return $msg;
			}
	    }
	    catch (Exception $exc) {
	    	DB::rollBack();
	    	$exc->getMessage();
	    }
	}

	//Get Particular Employee Details
	public function validateLogin($params)
	{
		$empModel = new EmployeeTable();
        $result = $empModel->validateLogin($params);
        return $result;
	}

	public function updatePassword($params,$id)
    {
    	DB::beginTransaction();
    	try {
			$model = new EmployeeTable();
			$result = $model->updatePassword($params,$id);
			if($result>0){
				DB::commit();
				$msg = 'Password Updated Successfully';
				return $msg;
			}else{
				// dd('addAgentKeyPerson');
				$msg = '1';
				return $msg;
			}
	    }
	    catch (Exception $exc) {
	    	DB::rollBack();
	    	$exc->getMessage();
	    }
	}
}

?>