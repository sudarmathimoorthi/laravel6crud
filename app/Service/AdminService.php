<?php

namespace App\Service;
use App\Model\Admin\AdminTable;
// use App\EventLog\EventLog;
use DB;
use Redirect;

class AdminService
{
   
    public function saveAdmin($request)
    {
    	$data =  $request->all();
    	DB::beginTransaction();
    	try {
			$modal = new AdminTable();
        	$addAdmin = $modal->saveAdmin($request);
			if($addAdmin>0){
				DB::commit();
				$msg = 'Admin Added Successfully';
				return $msg;
			}
	    }
	    catch (Exception $exc) {
	    	DB::rollBack();
	    	$exc->getMessage();
	    }
	}
	
	//Get All Admin List
	public function displayAllAdmin()
    {
		$modal = new AdminTable();
        $result = $modal->displayAllAdmin();
        return $result;
	}
	//Get Particular Admin Details
	public function getAdminById($id)
	{
		$modal = new AdminTable();
        $result = $modal->fetchAdminById($id);
        return $result;
	}
	//Update Particular Admin Details
	public function updateAdmin($params,$id)
    {
    	DB::beginTransaction();
    	try {
			$modal = new AdminTable();
        	$editAdmin = $modal->updateAdmin($params,$id);
			if($editAdmin>0){
				DB::commit();
				$msg = 'Admin Updated Successfully';
				return $msg;
			}
	    }
	    catch (Exception $exc) {
	    	DB::rollBack();
	    	$exc->getMessage();
	    }
	}

	//Get Particular Admin Details
	public function validateLogin($params)
	{
		$modal = new AdminTable();
        $result = $modal->validateLogin($params);
        return $result;
	}

	public function updatePassword($params,$id)
    {
    	DB::beginTransaction();
    	try {
			$model = new AdminTable();
			$result = $model->updatePassword($params,$id);
			if($result>0){
				DB::commit();
				$msg = 'Password Updated Successfully';
				return $msg;
			}else{
				// dd('addAgentKeyPerson');
				$msg = '1';
				return $msg;
			}
	    }
	    catch (Exception $exc) {
	    	DB::rollBack();
	    	$exc->getMessage();
	    }
	}
}

?>