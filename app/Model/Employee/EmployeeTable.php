<?php

namespace App\Model\Employee;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use App\Service\CommonService;
use Mail;
use DB;

class EmployeeTable extends Model
{
    protected $table = 'employee';


    public function saveEmployee($request)
    {
        //to get all request values
        $data = $request->all();
        // dd($data);
        $commonservice = new CommonService();
        if ($request->input('empName')!=null && trim($request->input('empName')) != '') {
            $validated = request()->validate([
                'file' => 'required|mimes:mp4,jpeg,jpg,png,pdf,xlsx,csv|max:10240',
                ]);
            
            $fileName = time().'.'.request()->file->getClientOriginalExtension();
            request()->file->move(public_path('uploads'), $fileName);
            $name = public_path('uploads').'/'.$fileName;
            $fileReturn = '/uploads/'.$fileName;
            // dd($fileReturn);
            $id = DB::table('employee')->insertGetId(
                ['employee_name' => $data['empName'],
                'employee_phno' => $data['empPhNo'],
                'employee_email' => $data['email'],
                'password' =>Hash::make($data['password']),
                'address' => $data['address'],
                'pincode' => $data['pincode'],
                'date_time' => $data['dateTime'],
                'created_on' => $commonservice->getDateTime(),
                // 'created_by' => session('userId'),
                'employee_status' => $data['empStatus'],
                'employee_img' => $fileReturn
                ]
            );
        }
        // dd($id);
        return $id;
    }

    // display All Employee List
    public function displayAllEmployee($params)
    {
        $data = $params->all();
        $query = DB::table('employee');
        $result  = $query->get();
        return $result;
    }

    // fetch particular Employee details
    public function fetchEmployeeById($id)
    {
        $id = base64_decode($id);
        $data = DB::table('employee')->where('employee_id', '=',$id )->get();
        return $data;
    }

    // Update particular Employee details
    public function updateEmployee($params,$id)
    {
        $data = $params->all();
        // dd($data);
        $commonservice = new CommonService();
        if ($params->input('empName')!=null && trim($params->input('empName')) != '') {
            if(isset($data['file'])){
                $validated = request()->validate([
                    'file' => 'required|mimes:mp4,jpeg,jpg,png,pdf,xlsx,csv|max:10240',
                    ]);
                $fileName = time().'.'.request()->file->getClientOriginalExtension();
                request()->file->move(public_path('uploads'), $fileName);
                $name = public_path('uploads').'/'.$fileName;
                $fileReturn = '/uploads/'.$fileName;
            }
            else{
                $fileReturn = $data['personimgdata'];
            }
            $response = DB::table('employee')
                ->where('employee_id', '=',$id)
                ->update(
                    ['employee_name' => $data['empName'],
                    'employee_phno' => $data['empPhNo'],
                    'employee_email' => $data['email'],
                    'updated_on' => $commonservice->getDateTime(),
                    'updated_by' => session('userId'),
                    'employee_status' => $data['empStatus'],
                    'address' => $data['address'],
                    'pincode' => $data['pincode'],
                    'date_time' => $data['dateTime'],
                    'employee_img' => $fileReturn
                    ]
                );
        }
        return $response;
    }

    // Validate Employee login
    public function validateLogin($params)
    {
        $data = $params->all();
        $result = json_decode(DB::table('employee')
        ->where('employee_phno','=', $data['email'])
        ->where('employee_status','=','1')->get(),true);
        if(count($result)>0)
        {
            $hashedPassword = $result[0]['password'];
            if (Hash::check($data['password'], $hashedPassword)) {
                session(['name' => $result[0]['employee_name']]);
                session(['mobileNo' => $result[0]['employee_phno']]);
                session(['email' => $result[0]['employee_email']]);
                session(['userId' => $result[0]['employee_id']]);
                session(['login' => true]);
                session(['role' => 'employee']);
                session(['userImg' => $result[0]['employee_img']]);
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            $result = json_decode(DB::table('admin')
            ->where('admin_mobile', '=',$data['email'] )
            ->where('admin_status','=','1')->get(),true);
            if(count($result)>0)
            {
                $hashedPassword = $result[0]['admin_password'];
                if (Hash::check($data['password'], $hashedPassword)) {
                    session(['name' => $result[0]['admin_name']]);
                    session(['mobileNo' => $result[0]['admin_mobile']]);
                    session(['email' => $result[0]['admin_email']]);
                    session(['userId' => $result[0]['admin_id']]);
                    session(['login' => true]);
                    session(['role' => 'admin']);
                        return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
    }

    //Update Password
    public function updatePassword($params,$id)
    {
        $data = $params->all();
        $newPassword= Hash::make($data['newPass']);
        $result = json_decode(DB::table('employee')->where('employee_id', '=',base64_decode($id) )->get(),true);
        if(count($result)>0)
        {
            $hashedPassword = $result[0]['password'];
            if (Hash::check($data['oldPass'], $hashedPassword)) {
                $response = DB::table('employee')
                ->where('employee_id', '=',base64_decode($id))
                ->update([
                    'password'=> $newPassword   
                    ]
                );
                return $response;
            }
        }
        else
        {
            return 0;
        }
    }
}
