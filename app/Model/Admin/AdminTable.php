<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Mail;
use DB;

class AdminTable extends Model
{
    protected $table = 'admin';


    public function saveAdmin($request)
    {
        //to get all request values
        $data = $request->all();
        // dd($data);
        if ($request->input('adminName')!=null && trim($request->input('adminName')) != '') {
            $validated = request()->validate([
                'file' => 'required|mimes:mp4,jpeg,jpg,png,pdf,xlsx,csv|max:10240',
                ]);
            
            $fileName = time().'.'.request()->file->getClientOriginalExtension();
            request()->file->move(public_path('uploads'), $fileName);
            $name = public_path('uploads').'/'.$fileName;
            $fileReturn = '/uploads/'.$fileName;
            $id = DB::table('admin')->insertGetId(
                ['admin_name' => $data['adminName'],
                'admin_mobile' => $data['adminPhNo'],
                'admin_email' => $data['email'],
                'admin_password' =>Hash::make($data['password']),
                'address' => $data['address'],
                'pincode' => $data['pincode'],
                'date_time' => $data['dateTime'],
                'admin_file' => $fileReturn,
                'admin_status' => $data['adminStatus']]
            );
        }
        return $id;
    }

    // display All Admin List
    public function displayAllAdmin()
    {
        $data = DB::table('admin')
                ->get();
        return $data;
    }

    // fetch particular Admin details
    public function fetchAdminById($id)
    {
        $id = base64_decode($id);
        $data = DB::table('admin')->where('admin_id', '=',$id )->get();
        return $data;
    }

    // Update particular Admin details
    public function updateAdmin($params,$id)
    {
        $data = $params->all();
        // dd($data);
        if ($params->input('adminName')!=null && trim($params->input('adminName')) != '') {
            if(isset($data['file'])){
                $validated = request()->validate([
                    'file' => 'required|mimes:mp4,jpeg,jpg,png,pdf,xlsx,csv|max:10240',
                    ]);
                
                $fileName = time().'.'.request()->file->getClientOriginalExtension();
                request()->file->move(public_path('uploads'), $fileName);
                $name = public_path('uploads').'/'.$fileName;
                $fileReturn = '/uploads/'.$fileName;
            }
            else{
                $fileReturn = $data['personimgdata'];
            }
            // dd($id);
            $response = DB::table('admin')
                ->where('admin_id', '=',$id)
                ->update(
                    ['admin_name' => $data['adminName'],
                    'admin_mobile' => $data['adminPhNo'],
                    'admin_email' => $data['email'],
                    'address' => $data['address'],
                    'pincode' => $data['pincode'],
                    'date_time' => $data['dateTime'],
                    'admin_file' => $fileReturn,
                    ]
                );
        }
        return $response;
    }

    // Validate Admin login
    public function validateLogin($params)
    {
        $data = $params->all();
        $result = json_decode(DB::table('admin')
        ->join('admin_role', 'admin_role.admin_role_id', '=', 'admin.admin_role_id')
        ->where('admin_mobile', '=',$data['email'] )
        ->where('admin_status','=','1')->get(),true);
        if(count($result)>0)
        {
            $hashedPassword = $result[0]['admin_password'];
            if (Hash::check($data['password'], $hashedPassword)) {
                $configFile =  "adminacl.config.json";
                if(file_exists(getcwd() . DIRECTORY_SEPARATOR . $configFile))
                {
                    $config = json_decode(File::get( getcwd() . DIRECTORY_SEPARATOR . $configFile),true);
                    session(['name' => $result[0]['admin_name']]);
                    session(['mobileNo' => $result[0]['admin_mobile']]);
                    session(['email' => $result[0]['admin_email']]);
                    session(['userId' => $result[0]['admin_id']]);
                    session(['userType' => 'admin']);
                    session(['login' => true]);
                    session(['role' => $config[$result[0]['admin_role_id']]]);
                    return 1;
                }
                else
                {
                    return 2;
                }
            }


        }
        else
        {
            return 0;
        }
    }
    //Update Password
    public function updatePassword($params,$id)
    {
        $data = $params->all();
        $newPassword= Hash::make($data['newPass']);
        $result = json_decode(DB::table('admin')->where('admin_id', '=',base64_decode($id) )->get(),true);
        if(count($result)>0)
        {
            $hashedPassword = $result[0]['admin_password'];
            if (Hash::check($data['oldPass'], $hashedPassword)) {
                $response = DB::table('admin')
                ->where('admin_id', '=',base64_decode($id))
                ->update([
                    'admin_password'=> $newPassword   
                    ]
                );
                return $response;
            }
        }
        else
        {
            return 0;
        }
    }
}
