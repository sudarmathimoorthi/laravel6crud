<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class ItemPriceCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'itemprice:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Item Price Add ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        DB::beginTransaction();
    	try {
            $shop = DB::table('shop_branch')
                    ->join('shop', 'shop.shop_id', '=', 'shop_branch.shop_id')
                    ->get()
                    ;
            $today_date = date('Y-m-d');
            // $today_date = "2020-02-07";
            $yesterday_date = date('Y-m-d',strtotime("-1 days"));
            // \Log::info($yesterday_date);

            // \Log::info(count($todayGradePriceCount));
            for($i=0;$i<count($shop);$i++)
            {
                $item = DB::table('item')
                    ->where('shop_id','=',$shop[$i]->shop_id)
                    ->get()
                    ;
                for($j=0;$j<count($item);$j++)
                {
                    $todayGradePriceCount =DB::table('item_price')
                                ->where('created_on', '=',$today_date )
                                ->where('item_id','=',$item[$j]->item_id)
                                ->where('shop_id','=',$shop[$i]->shop_id)
                                ->where('shop_branch_id','=',$shop[$i]->shop_branch_id)
                                ->get()
                                ;
                    if(count($todayGradePriceCount)==0)     //Grade price not updated for current day
                    {
                        $gradePriceData = "";
                        $gradePriceData = DB::table('item_price')
                                        ->where('created_on', '=',$yesterday_date)
                                        ->where('item_id','=',$item[$j]->item_id)
                                        ->where('shop_id','=',$shop[$i]->shop_id)
                                        ->where('shop_branch_id','=',$shop[$i]->shop_branch_id)
                                        ->get()
                                        ;
                        if(count($gradePriceData)>0)
                        {
                            $insertGradePrice = DB::table('item_price')->insertGetId(
                                [
                                'item_id' => $gradePriceData[0]->item_id,
                                'shop_id' => $gradePriceData[0]->shop_id,
                                'shop_branch_id' => $gradePriceData[0]->shop_branch_id,
                                'item_price' => $gradePriceData[0]->item_price,
                                'created_on' => $today_date,
                                'cost_price' => $gradePriceData[0]->cost_price
                                ]
                            );
                            \Log::info('Inserted The grade price data with Yesterday value for grade id - '.$gradePriceData[0]->item_id."  val  - ".$insertGradePrice);
                        }
                        else
                        {
                            $insertGradePrice = DB::table('item_price')->insertGetId(
                                [
                                'item_id' => $item[$j]->item_id,
                                'shop_id' => $shop[$i]->shop_id,
                                'shop_branch_id' => $shop[$i]->shop_branch_id,
                                'item_price' => 0,
                                'created_on' => $today_date,
                                'cost_price' => 0
                                ]
                            );
                            \Log::info('Inserted The grade price data with Yesterday value for grade id - '.$item[$j]->item_id."  val  - ".$insertGradePrice);
                        }
                    }
                    else                                    //Grade price  updated for current day
                    {
                        \Log::info("Price list is already ran for item - ".$item[$j]->item_id);
                    }
                }
            }
        }
        catch (Exception $exc) {
            DB::rollBack();
            $exc->getMessage();
        }
        DB::commit();
        // return $data;
        /*
           Write your database logic we bellow:
           Item::create(['name'=>'hello new']);
        */
      
    }
}
