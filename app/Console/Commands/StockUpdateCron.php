<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class StockUpdateCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stockupdate:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stock Update ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        DB::beginTransaction();
    	try {
            \Log::info("Stock update");
            $shop = DB::table('shop_branch')
                    ->join('shop', 'shop.shop_id', '=', 'shop_branch.shop_id')
                    ->get()
                    ;
            $today_date = date('Y-m-d');
            // $today_date = "2020-02-27";
            $yesterday_date = date('Y-m-d',strtotime("-1 days"));

            for($i=0;$i<count($shop);$i++)
            {
                $item = DB::table('item')
                    ->where('shop_id','=',$shop[$i]->shop_id)
                    ->get()
                    ;
                for($j=0;$j<count($item);$j++)
                {
                    $shopStockData = DB::table('item_stock')
                                    ->where('stock_date', '=',$today_date)
                                    ->where('item_id','=',$item[$j]->item_id)
                                    ->where('shop_id','=',$shop[$i]->shop_id)
                                    ->where('shop_branch_id','=',$shop[$i]->shop_branch_id)
                                    ->get()
                                    ;
                    if(count($shopStockData)==0)
                    {
                        $shopStockYesterdayData = DB::table('item_stock')
                                    ->where('stock_date', '=',$yesterday_date)
                                    ->where('item_id','=',$item[$j]->item_id)
                                    ->where('shop_id','=',$shop[$i]->shop_id)
                                    ->where('shop_branch_id','=',$shop[$i]->shop_branch_id)
                                    ->get()
                                    ;
                        if(count($shopStockYesterdayData)>0)
                        {
                            $yesterdayOpeningBal = $shopStockYesterdayData[0]->opening_stock;
                            $yesterdayInwards = $shopStockYesterdayData[0]->inwards;
                            $yesterdayOutwards = $shopStockYesterdayData[0]->outwards;
                            $yesterdayClosingBal = ($yesterdayOpeningBal + $yesterdayInwards) - $yesterdayOutwards;
                            $yesterdayCloseBalUpdate = DB::table('item_stock')
                                ->where('item_stock_id','=', $shopStockYesterdayData[0]->item_stock_id)
                                ->update(
                                    [
                                        'closing_stock' => $yesterdayClosingBal
                                    ]
                                );
                            
                                $todayshopStockInsert = DB::table('item_stock')->insertGetId(
                                    [
                                    'shop_id' => $shop[$i]->shop_id,
                                    'shop_branch_id' => $shop[$i]->shop_branch_id,
                                    'item_id' => $item[$j]->item_id,
                                    'stock_date' => $today_date,
                                    'opening_stock' => $yesterdayClosingBal,
                                    'closing_stock' => 0,
                                    'inwards' => 0,
                                    'outwards' => 0,
                                    'latest_stock' => 0,
                                    ]
                                );
                                \Log::info("Stock update with yesterday data for Item - ".$item[$j]->item_id.", shop - ".$shop[$i]->shop_id);
                        }
                        else
                        {
                            $todayStockInsert = DB::table('item_stock')->insertGetId(
                                [
                                'shop_id' => $shop[$i]->shop_id,
                                'shop_branch_id' => $shop[$i]->shop_branch_id,
                                'item_id' => $item[$j]->item_id,
                                'stock_date' => $today_date,
                                'opening_stock' => 0,
                                'closing_stock' => 0,
                                'inwards' => 0,
                                'outwards' => 0,
                                'latest_stock' => 0,
                                ]
                            );
                            \Log::info("Stock update without yesterday data for Item - ".$item[$j]->item_id.", shop - ".$shop[$i]->shop_id);
                        }
                    }
                    else
                    {
                        \Log::info("Stock update is already ran  today for- ".$shopStockData[0]->item_stock_id);
                    }
                }

            }
        }
        catch (Exception $exc) {
            DB::rollBack();
            $exc->getMessage();
        }
        DB::commit();
        // return $data;
        /*
           Write your database logic we bellow:
           Item::create(['name'=>'hello new']);
        */
      
    }
}
