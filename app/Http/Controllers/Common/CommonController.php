<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service\CommonService;
use App\Service\ItemService;
use File;

class CommonController extends Controller
{
   

    public function checkNameValidation(Request $request)
    {
        $commonService = new CommonService();
        $data = $commonService->checkNameValidation($request);
        return $data;
    }


    public function imageupload()
    {
        $imgFolder =  public_path('images');

        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            // dd($imageName);
            // dd(public_path('images'));
        request()->image->move(public_path('images'), $imageName);
        $name = public_path('images').'/'.$imageName;
        $imageReturn = 'images/'.$imageName;
        return $imageReturn;
        // return back()->with('success','You have successfully upload image.')->with('image',$imageName);

    }
}
