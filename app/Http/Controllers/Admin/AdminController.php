<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service\AdminRoleService;
use App\Service\AdminService;
use Yajra\DataTables\Facades\DataTables;
use Redirect;
use Session;

class AdminController extends Controller
{
    //
    //View Admin main screen
    public function index()
    {
       if(session('login')==true)
       {
           return view('admin.index');
       }
       else
           return Redirect::to('login')->with('status', 'Please Login');
    }

    //Add Admin (display add screen and add the Admin values)
    public function add(Request $request)
    {
        if ($request->isMethod('post')) 
        {
            $service = new AdminService();
            $serviceDetails = $service->saveAdmin($request);
            return Redirect::route('admin.index')->with('status', $serviceDetails);
        }
        else
        {
            
            return view('admin.add');
        }
    }

    // Get all the Admin list
    public function displayAllAdmin(Request $request)
    {
        $service = new AdminService();
        $data = $service->displayAllAdmin();
        return DataTables::of($data)
                    ->editColumn('admin_status', function($data){
                        if($data->admin_status == 1){
                            $status = 'Active';
                            return $status;
                        }
                        else{
                            $status = 'Inactive';
                            return $status;
                        }
                    })
                    ->addColumn('action', function($data){
                        $button = '<a href="/admin/edit/'. base64_encode($data->admin_id).'" name="edit" id="'.$data->admin_id.'" class="btn gradient-3 btn-sm" title="Edit"><i class="fa fa-edit"></i></a>';
                        return $button;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
    }

    //edit Admin
    public function edit($id,Request $request)
    {
        if ($request->isMethod('post')) 
        {
            $editService = new AdminService();
            $editDetails = $editService->updateAdmin($request,$id);
            return Redirect::route('admin.index')->with('status', $editDetails);
        }
        else
        {
            $editService = new AdminService();
            $editServiceDetails = $editService->getAdminById($id);
            
            return view('admin.edit',array('admin'=>$editServiceDetails));
        }
    }

    public function changePassword($id,Request $request)
    {
        if ($request->isMethod('post')) 
        {
            $service = new AdminService();
            $result = $service->updatePassword($request,$id);
            if($result!=1){
                return Redirect::to('/dashboard');
            }else{
                
                return view('admin.changepassword',array('id'=>$id,'status'=>'Currently you entered incorrect password'));
            }
        }
        else
        {
            return view('admin.changepassword',array('id'=>$id));
        }
    }

    
}
