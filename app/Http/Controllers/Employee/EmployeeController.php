<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service\RoleService;
use App\Service\ShopService;
use App\Service\EmployeeService;
use Yajra\DataTables\Facades\DataTables;
use Redirect;
use Session;

class EmployeeController extends Controller
{
    //
    //View employee main screen
    public function index()
    {
       if(session('login')==true)
       {
           return view('employee.index');
       }
       else
           return Redirect::to('login')->with('status', 'Please Login');
    }

    //Add Employee (display add screen and add the Employee values)
    public function add(Request $request)
    {
        if ($request->isMethod('post')) 
        {
            $addEmployee = new EmployeeService();
            $addEmployeeDetails = $addEmployee->saveEmployee($request);
            return Redirect::route('employee.index')->with('status', $addEmployeeDetails);
        }
        else
        {
            return view('employee.add');
        }
    }

    // Get all the Employee list
    public function displayAllEmployee(Request $request)
    {
        $empService = new EmployeeService();
        $data = $empService->displayAllEmployee($request);
        return DataTables::of($data)
                    ->editColumn('employee_status', function($data){
                        if($data->employee_status == 1){
                            $status = 'Active';
                            return $status;
                        }
                        else{
                            $status = 'Inactive';
                            return $status;
                        }
                    })
                    ->addColumn('action', function($data){
                        $button = '';
                        if(session('role') == 'admin')
                        $button = '<a href="/employee/edit/'. base64_encode($data->employee_id).'" name="edit" id="'.$data->employee_id.'" class="btn gradient-3 btn-sm" title="Edit"><i class="fa fa-edit"></i></a>';
                        return $button;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
    }

    //edit Employee
    public function edit($id,Request $request)
    {
        if ($request->isMethod('post')) 
        {
            $editEmployee = new EmployeeService();
            $editEmployeeDetails = $editEmployee->updateEmployee($request,$id);
            return Redirect::route('employee.index')->with('status', $editEmployeeDetails);
        }
        else
        {
            $editEmployee = new EmployeeService();
            $editEmployeeDetails = $editEmployee->getEmployeeById($id);
            return view('employee.edit',array('emp'=>$editEmployeeDetails));
        }
    }

    // Get all the Role list
    public function displayRoleForEmployee(Request $request)
    {
        $service = new RoleService();
        $data = $service->getActiveRoles($request);
        return $data;
    }


    public function changePassword($id,Request $request)
    {
        if ($request->isMethod('post')) 
        {
            $service = new EmployeeService();
            $result = $service->updatePassword($request,$id);
            if($result!=1){
                return Redirect::to('/dashboard');
            }else{
                
                return view('employee.changepassword',array('id'=>$id,'status'=>'Currently you entered incorrect password'));
            }
        }
        else
        {
            return view('employee.changepassword',array('id'=>$id));
        }
    }

    
}
