<?php

namespace App\Http\Controllers\AdminLogin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service\AdminService;
use Yajra\DataTables\Facades\DataTables;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Session;
// use App\Service\CommonService;

class AdminLoginController extends Controller
{
    //View AdminLogin main screen
     public function index()
     {
         return view('adminlogin.index');
     }

     //validate login
     public function validateLogin(Request $request)
    {
            $service = new AdminService();
            $login = $service->validateLogin($request);
            if(trim($login)==1)
            {
                return Redirect::to('/dashboard');
            }
            elseif(trim($login)==2)
            { 
                return Redirect::route('login.index')->with('status', 'No Role Available, Please Contact Admin!');
            }
            else
            { 
                return Redirect::route('adminlogin.index')->with('status', 'Login Failed');
            }
    }


    //Logout
    public function adminlogout(Request $request){
        if($request->isMethod('post') && session('login')==true){
            $request->session()->flush();
            $request->session()->regenerate();
            
            Session::flush();
            return Redirect::to('/adminlogin'); // redirect the user to the adminlogin screen
        }else{
            if(session('login')== null){
                return Redirect::to('/adminlogin');
            }
            return redirect()->back();
        }
    }
 
    
}
