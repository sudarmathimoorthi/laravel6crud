<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service\EmployeeService;
use Yajra\DataTables\Facades\DataTables;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Session;
// use App\Service\CommonService;

class LoginController extends Controller
{
    //View Login main screen
     public function index()
     {
         return view('login.index');
     }

     //validate login
     public function validateLogin(Request $request)
    {
            $service = new EmployeeService();
            $login = $service->validateLogin($request);
            if(trim($login)==1)
            {
                return Redirect::to('/employee');
            }
            else
            { 
                return Redirect::route('login.index')->with('status', 'Login Failed');
            }
    }


    //Logout
    public function logout(Request $request){
        if($request->isMethod('post') && session('login')==true){
            $request->session()->flush();
            $request->session()->regenerate();
            
            Session::flush();
            return Redirect::to('/login'); // redirect the user to the login screen
        }else{
            if(session('login')== null){
                return Redirect::to('/login');
            }
            return redirect()->back();
        }
    }
 
    
}
