<!--**********************************
    Sidebar start
***********************************-->

<?php 
use Illuminate\Support\Facades\Request;

$role = session('role');
$manage = '';
// if (isset($role['App\\Http\\Controllers\\Plan\\PlanController']['index']) ||isset($role['App\\Http\\Controllers\\Customer\\CustomerController']['index']) || isset($role['App\\Http\\Controllers\\AdminRole\\AdminRoleController']['index']) || isset($role['App\\Http\\Controllers\\Admin\\AdminController']['index']) || isset($role['App\\Http\\Controllers\\Role\\RoleController']['index']) || isset($role['App\\Http\\Controllers\\Shop\\ShopController']['index']) || isset($role['App\\Http\\Controllers\\Shop\\ShopController']['shopbranch']) || isset($role['App\\Http\\Controllers\\Employee\\EmployeeController']['index']) || isset($role['App\\Http\\Controllers\\Supplier\\SupplierController']['index']) )
// {
    $manage.= ' <li>';
    $manage.= ' <a class="has-arrow" id="master"  aria-expanded="false"><i class="icon-speedometer menu-icon"></i><span class="nav-text">Masters</span></a> <ul aria-expanded="false" id="masterul">';
    if ($role=="admin")
        $manage.= '<li ><a href="/admin" id="admin">Admin</a></li>';
    // if (isset($role['App\\Http\\Controllers\\Employee\\EmployeeController']['index']) && ($role['App\\Http\\Controllers\\Employee\\EmployeeController']['index'] == "allow"))
        $manage.= '<li ><a href="/employee" id="employee">User</a></li>';
    $manage.= '</ul></li>';
// }

?>

<div class="nk-sidebar" >           
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">
            
            <?php echo $manage; ?>
        </ul>
    </div>
</div>
<!--**********************************
    Sidebar end
***********************************-->
