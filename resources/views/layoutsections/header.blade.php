
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="csrf-token" content="{{csrf_token()}}">
<title>Laravel 6 CRUD with 2 Login</title>
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16" href="">
<!-- Pignose Calender -->
<link href="{{ asset('assets/plugins/pg-calendar/css/pignose.calendar.min.css')}}" rel="stylesheet">
<!-- Custom Stylesheet -->
<link href="{{ asset('assets/css/style.css')}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/tables/css/datatable/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
<!-- <link href="{{ asset('assets/plugins/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet"> -->
<link href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
<link href="{{ asset ('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
<link href="{{ asset ('assets/plugins/sweetalert/css/sweetalert.css')}}" rel="stylesheet">
<script src="{{ asset('assets/plugins/common/common.min.js')}}"></script>