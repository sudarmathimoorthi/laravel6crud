
@extends('layouts.main')

@section('content')
<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">

            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item active"><a href="/employee/changePassword/{{$id}}">Change Password</a></li>
        </ol>
    </div>
</div>

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                
                <div class="card-header mt-2">
                    <h3>Employee Change Password</h3>
                </div>
                <div class="alert alert-danger alert-dismissible fade show ml-5 mr-5 mt-2" id="showAlertdiv" role="alert" style="display:none"><span id="showAlertIndex"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="card-body">
				<div id="show_alert"  class="mt-1" style=""></div>
				@if (isset($status))
                          <div class="alert alert-success alert-dismissible fade show ml-5 mr-5 mt-1" role="alert" id="show_alert_index" ><div class="text-center" style="font-size: 18px;"><b>
                              {{ $status }}</b></div>
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                          </div>
                          <script>$('#show_alert_index').delay(3000).fadeOut();</script>
                      @endif
                    <div class="form-validation">
                        <form id="changepassword" name="changepassword" action="/employee/changePassword/{{$id}}" method="post" onsubmit="validateNow();return false;">
                        @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="oldPass">Old Password <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" class="form-control col-md-11 isRequired" id="oldPass" name="oldPass" title="Please Enter Old Password" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="newPass">New Password <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" class="form-control col-md-11 isRequired" id="newPass"  name="newPass" title="Please Enter New Password" autocomplete="off" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="confPass">Confirm Password <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" class="form-control col-md-11 isRequired" id="confPass" name="confPass" title="Please Enter Confirm Password" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label"><a href="#">Required Fields</a>  <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn gradient-3 float-right ml-3">Submit</button>
                                    <a href="/dashboard" class="btn btn-primary float-right">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
 duplicateName = true;
    function validateNow() {
        $.blockUI();
        flag = validator.init({
            formId: 'changepassword'
		});
		if(flag==true)
        {
            if($("#newPass").val() != $("#confPass").val()) {
                flag='<div class="alert alert-danger alert-dismissible fade show ml-5 mr-5 mt-2" role="alert" ><div class="text-center" style="font-size: 18px;"><b>Password do not match</b></div>';
                flag+='<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>';
                role = 1;
            } else {
                flag = true;
            }
        }
        if (flag == true) {
            if (duplicateName) {
                document.getElementById('changepassword').submit();
            }
        }
        else{
            $.unblockUI();
            // Swal.fire('Any fool can use a computer');
            $('#show_alert').html(flag).delay(3000).fadeOut();
			$('#show_alert').css("display","block");
			$(".infocus").focus();
        }
    }



	// function passwordStrengthvalidation(passwordElementObject){
    //     const validPasswordRegEx = new RegExp("^(?=.*[a-z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,})|(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,})");
    //     if(validPasswordRegEx.test(passwordElementObject.value) === false){
    //         $("#"+passwordElementObject.id).val("");
    //         $(".invalid-feedback").show();
    //     }else{
    //         $(".invalid-feedback").hide();
    //     }
    // }
	



</script>
@endsection