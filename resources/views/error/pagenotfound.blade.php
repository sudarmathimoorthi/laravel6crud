<style>
.app-header{
  align-items: flex-start;
    display: flex;
    flex-direction: column;
    margin: 20px 20px 0;
}
#content {
    display: flex;
    flex-direction: column;
    flex: auto;
    width: 100%;
    padding-left: 20px;
    padding-right: 20px;
    clear: both;
    position: relative;
    padding: 0;
}
#content, .aui-group>.aui-item {
    margin: 0;
    box-sizing: border-box;
}

#content .aui-page-panel, #content .aui-page-panel-inner {
    min-height: 400px;
}

.aui-page-panel-inner {
    border-spacing: 0;
    display: table;
    table-layout: fixed;
    width: 100%;
}
#content .aui-page-panel-inner {
    min-height: 400px;
}
#error {
    padding: 128px 0;
    text-align: center;
}
#error .icon {
    background: url(../../img/errors/error-illustration.svg) no-repeat;
    background-size: contain;
    display: block;
    margin: 0 auto;
    height: 200px;
    width: 200px;
}
#error p {
    font-size: 14px;
    line-height: 24px;
}
</style>
<div id="content" role="main">
        
          <header class="app-header">
              <div class="app-header--primary">
                
                  <div class="app-header--context">
                    <div class="app-header--breadcrumbs">
                      
                    </div>
                      
                  </div>
                
             </div>
             <div class="app-header--secondary">
                <h1 class="app-header--heading">
                  
                </h1>
               
                 
               
             </div>
          </header>
        
        
        
  <div class="aui-page-panel aui-page-panel-no-header">
    <div class="aui-page-panel-inner">
      <div id="error" class="power_off">
        <span class="icon icon-404">
        <img src="{{ asset('assets/images/nolink.png') }}" alt="" style="width:200px; height:100px">
        </span>
        <h1>
          That link has no power here
        </h1>
        <p>
          
            Return to the <a href="/dashboard">Dashboard</a> or
            go back to Login <a href="/login">Login</a>.
          
        </p>
      </div>
    </div>
  </div>

      </div>