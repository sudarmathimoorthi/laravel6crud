<!DOCTYPE html>
<html>
  <head>
  
  @include('layoutsections.header')

  </head>
  <style>
  .mandatory{color: #f30f00;}
  .logostyle{
    color:#fff;font-size: 30px;!important;
  }
  .firstcaps{
    text-transform:capitalize;
  }
  body{
      margin-top : 30px;
  }
  </style>


<body class="h-100" data-theme-version="light" data-layout="vertical" data-nav-headerbg="color_1" data-headerbg="color_1" data-sidebar-style="full" data-sibebarbg="color_1" data-sidebar-position="static" data-header-position="static" data-container="wide" direction="ltr">
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader" style="display: none;">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"></circle>
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    



    <div class="login-form-bg h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                @if (session('status'))
                          <div class="alert alert-danger alert-dismissible fade show ml-5 mr-5 mt-1" role="alert" id="show_alert_index" ><div class="text-center" style="font-size: 18px;"><b>
                              {{ session('status') }}</b></div>
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                          </div>
                          <script>$('#show_alert_index').delay(3000).fadeOut();</script>
                      @endif
                    <div class="form-input-content">
                        <div class="card login-form mb-0">
                            <div class="card-body pt-5">
                            <div class="card-title text-center">
                                </div>
                                <a class="text-center" href="/login"> <h4>Laravel 6 CRUD</h4></a>
                                <div id="show_alert"  class="mt-4" style=""></div>
                                <form class="mt-5 mb-5 login-input" id="loginValidate" name="loginValidate" action="login/validate" method="post" onsubmit="validateNow();return false;">
                                @csrf
                                    <div class="form-group">
                                    <label class="" for="email">Mobile Number <span class="text-danger">*</span>
                                        </label>
                                        <input type="tel" name="email" class="form-control isRequired" placeholder="Enter the Mobile Number" title="Please Enter Mobile Number">
                                    </div>
                                    <div class="form-group">
                                    <label class="" for="password">Password <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" name="password" class="form-control isRequired" placeholder="Enter the Password" title="Please Enter Password">
                                    </div>
                                    <button class="btn login-form__btn submit w-100">Sign In</button>
                                </form>
                                <!-- <p class="mt-5 login-form__footer">Dont have account? <a href="page-register.html" class="text-primary">Sign Up</a> now</p> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script> -->
    <script src="{{ asset('assets/js/prasudValidation.js')}}"></script>
    <script src="{{ asset('assets/js/jquery.blockUI.js')}}"></script>
    <script>
duplicateName = true;
    function validateNow() {
        $.blockUI();
        flag = validator.init({
            formId: 'loginValidate'
        });
        console.log(flag);
        if (flag == true) {
            if (duplicateName) {
                document.getElementById('loginValidate').submit();
            }
        }
        else{
            $.unblockUI();
            // Swal.fire('Any fool can use a computer');
            $('#show_alert').html(flag).delay(3000).fadeOut();
            $('#show_alert').css("display","block");
        }
    }

</script>





</body>