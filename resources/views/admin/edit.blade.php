
@extends('layouts.main')

@section('content')

<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">

            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item active"><a href="/admin">Admin</a></li>
            <li class="breadcrumb-item">Edit</li>
        </ol>
    </div>
</div>
<!-- row -->

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header mt-2">
                    <h3>Edit Admin Details</h3>
                </div>
                <div class="alert alert-danger alert-dismissible fade show ml-5 mr-5 mt-2" id="showAlertdiv" role="alert" style="display:none"><span id="showAlertIndex"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="card-body">
                <div id="show_alert"  class="mt-4" style=""></div>
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <div class="form-validation">
                        <form id="editAdmin" name="editAdmin" enctype="multipart/form-data" action="/admin/edit/{{$admin[0]->admin_id}}" method="post" onsubmit="validateNow();return false;">
                        @csrf
                        @php
                            $fnct = "admin_id##".($admin[0]->admin_id);
                        @endphp
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="adminName">admin Name <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" onkeypress="return isChar(event);" class="form-control col-md-11 isRequired" value="{{$admin[0]->admin_name}}" id="adminName" name="adminName" title="Please Enter admin Name" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="adminPhNo">admin Mobile <span class="text-danger">*</span>
                                        </label>
                                        <input type="tel" onkeypress="return isNumberKey(event);" class="form-control col-md-11 isRequired" value="{{$admin[0]->admin_mobile}}" id="adminPhNo" maxlength="10"  name="adminPhNo" title="Please Enter admin Phone Number" autocomplete="off" onblur="checkNameValidation('admin','admin_mobile', this.id,'{{$fnct}}','Entered admin mobile number is already exist.')">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="email">admin Email
                                        </label>
                                        <input type="text" class="form-control col-md-11 isEmail" id="email" value="{{$admin[0]->admin_email}}" name="email" title="Please Enter admin Valid Email" autocomplete="off" onblur="checkNameValidation('admin','admin_email', this.id,'{{$fnct}}','Entered admin email id is already exist.')" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="adminStatus">admin Status <span class="text-danger">*</span>
                                        </label>
                                        <select class="form-control col-md-11 isRequired" autocomplete="off" style="width:100%;" id="adminStatus" name="adminStatus" title="Please select status" disabled>
                                            <option value="1" {{ $admin[0]->admin_status == 1 ?  'selected':''}}>Active</option>
                                            <option value="0" {{ $admin[0]->admin_status == 0 ?  'selected':''}}>Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="address">Address<span class="text-danger">*</span>
                                        </label>
                                        <textarea class="col-md-11 isRequired" id="address" name="address" title="Please Enter Address" autocomplete="off">{{$admin[0]->address}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="pincode">Pincode <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" value="{{$admin[0]->pincode}}" onkeypress="return isNumberKey(event);" class="form-control col-md-11 isRequired" id="pincode" maxlength="6"  name="pincode" title="Please Enter Pincode" autocomplete="off" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="m-t-40">Date Timepicker</label>
                                    <input type="text" id="date-format" class="form-control" name="dateTime" value="{{$admin[0]->date_time}}">
                                </div>
                                <div class="col-md-4">
                                    <input type="hidden" name="personimgdata" id="personimgdata" value="{{$admin[0]->admin_file}}">
                                    File Upload <span class="text-danger">*</span>
                                    <?php if($admin[0]->admin_file) {
                                        $lbl = explode('/',$admin[0]->admin_file);
                                    ?>
                                        <div class="custom-file mt-2">
                                            <input type="file" class="custom-file-input" id="file" name="file">
                                            <label class="custom-file-label" for="file">{{$lbl[2]}}</label>
                                        </div>
                                    <?php } else { ?>
                                        <div class="custom-file mt-2">
                                            <input type="file" class="custom-file-input" id="file" name="file">
                                            <label class="custom-file-label" for="file">Choose file</label>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-4">
                                    <label>Preview</label><br>
                                    @php
                                        $ext = explode('.',$admin[0]->admin_file);
                                    @endphp
                                    @if($ext[1] == 'jpeg' || $ext[1] == 'png' || $ext[1] == 'jpg' || $ext[1] == 'gif')
                                        <img src="{{$admin[0]->admin_file}}" alt="Test" width="200" height="200">
                                    @else
                                        <a href="{{$admin[0]->admin_file}}" target="_blank"><img src="https://cdn.iconscout.com/icon/free/png-256/doc-file-75-898976.png" alt="Test" width="200" height="200"></a>
                                    @endif
                                
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label"><a href="#">Required Fields</a>  <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn gradient-3 float-right ml-3">Submit</button>
                                    <a href="/admin" class="btn btn-primary float-right">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/ container -->

<script>
    $(document).ready(function() {
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });

    });
    duplicateName = true;
    function validateNow() {
        $.blockUI();
        mobNum = $('#adminPhNo').val();
		if(mobNum.length!=10){
            $.unblockUI();
			$("html, body").animate({ scrollTop: 0 }, "slow");
            alert = '<div class="text-center" style="font-size: 18px;"><b>Please give 10  digit mobile number</b></div>'
			$("#showAlertIndex").html(alert);
			$('#showAlertdiv').show();
			ismob = false;
			$('#adminPhNo').css('background-color', 'rgb(255, 255, 153)')
			$('#adminPhNo').focus();
			$('#showAlertdiv').delay(3000).fadeOut();
		}
		else{
			ismob = true;
		}
        if(ismob==true){
            flag = validator.init({
                formId: 'editAdmin'
            });
            console.log(flag);
            if (flag == true) {
                if (duplicateName) {
                    document.getElementById('editAdmin').submit();
                }
            }
            else{
                $.unblockUI();
                // Swal.fire('Any fool can use a computer');
                $('#show_alert').html(flag).delay(3000).fadeOut();
                $('#show_alert').css("display","block");
            }
        }
    }

    function checkNameValidation(tableName, fieldName, obj, fnct, msg) {
		checkValue = document.getElementById(obj).value;
		if ($.trim(checkValue) != '') {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url: "{{ url('/checkNameValidation') }}",
				method: 'post',
				data: {
					tableName: tableName,
					fieldName: fieldName,
					value: checkValue,
					fnct: fnct,
				},
				success: function(result) {
					// console.log(result);
					if (result > 0) {
                        $("html, body").animate({ scrollTop: 0 }, "slow");
						$("#showAlertIndex").text(msg);
						$('#showAlertdiv').show();
						duplicateName = false;
						document.getElementById(obj).value = "";
						$('#' + obj).focus();
						$('#' + obj).css('background-color', 'rgb(255, 255, 153)')
						$('#showAlertdiv').delay(3000).fadeOut();
					} else {
						duplicateName = true;
					}
				}
			});
		}
	}

</script>
@endsection