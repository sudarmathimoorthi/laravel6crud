
@extends('layouts.main')

@section('content')

<div class="main-panel">
    <div class="content">
        <div class="container-fluid">    
              <div class="card ">
                  <div class="card-header mt-2">
                    <div class="row">
                        <div class="col-xl-10 col-lg-12">
                            <h3><a href="/admin" >Admin Details </a></h3>
                        </div>
                        <div class="col-xl-2 col-lg-12">
                            <a href="/admin/add" class="btn gradient-3 pull-right"><b>Add Admin</b></a>
                        </div>
                    </div>
                  </div>
                  <div class="card-body">
                      @if (session('status'))
                          <div class="alert alert-success alert-dismissible fade show ml-5 mr-5 mt-1" role="alert" id="show_alert_index" ><div class="text-center" style="font-size: 18px;"><b>
                              {{ session('status') }}</b></div>
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                          </div>
                          <script>$('#show_alert_index').delay(3000).fadeOut();</script>
                      @endif
                      <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration" id="adminList">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Mobile No</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                  </div>
              </div>
        </div>
    </div>
</div>

<script type="text/javascript">

  $(document).ready(function() {
    displayAllAdmin();
  });

  function displayAllAdmin()
  {
    $.blockUI();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#adminList').DataTable({
          processing: true,
          destroy : true,
          serverSide: true,
          scrollX: false,
          autoWidth:false,
          ajax: {
              url:'{{ url("displayAllAdmin") }}',
              type: 'POST',
          },
          columns: [
                  
                  { data: 'admin_name', name: 'admin_name',className:'firstcaps' },
                  { data: 'admin_mobile', name: 'admin_mobile' },
                  { data: 'admin_email', name: 'admin_email' },
                  { data: 'admin_status', name: 'admin_status' },
                  {data: 'action', name: 'action', orderable: false},
              ],
          order: [[0, 'desc']]
      });
      $.unblockUI();
  }
</script>
@endsection
