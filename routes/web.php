<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login.index');
});
Route::get('/pagenotfound', function () {
    return view('error/page-not-found');
});
Route::get('/unauthorized', function()
{
    return view('error.not-authorized');
});
Route::get('/incorrectrequest', function()
{
    return view('error.incorrect-request');
});

//login module
Route::get('/login', 'Login\LoginController@index')->name('login.index');
Route::post('/login/validate', 'Login\LoginController@validateLogin');
Route::match(['get','post'],'/logout', 'Login\LoginController@logout');


//employee module
Route::get('/employee', 'Employee\EmployeeController@index')->name('employee.index');
Route::post('/employee/add', 'Employee\EmployeeController@add');
Route::get('/employee/add', 'Employee\EmployeeController@add');
Route::post('/displayAllEmployee', 'Employee\EmployeeController@displayAllEmployee');
Route::post('/employee/edit/{id}', 'Employee\EmployeeController@edit');
Route::get('/employee/edit/{id}', 'Employee\EmployeeController@edit');

//common functions
Route::post('/checkNameValidation', 'Common\CommonController@checkNameValidation');
Route::post('/imageupload', 'Common\CommonController@imageupload');


//Admin module
Route::get('/admin', 'Admin\AdminController@index')->name('admin.index');
Route::post('/admin/add', 'Admin\AdminController@add');
Route::get('/admin/add', 'Admin\AdminController@add');
Route::post('/displayAllAdmin', 'Admin\AdminController@displayAllAdmin');
Route::post('/admin/edit/{id}', 'Admin\AdminController@edit');
Route::get('/admin/edit/{id}', 'Admin\AdminController@edit');

