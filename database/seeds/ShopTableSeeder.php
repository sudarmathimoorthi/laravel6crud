<?php

use Illuminate\Database\Seeder;

class ShopTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // For single
        // For multiple using factory model using helper method
            factory(App\Model\Shop\ShopTable::class,20)->create();

    }
}
