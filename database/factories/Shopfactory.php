<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Shop\ShopTable;
use Faker\Generator as Faker;

$factory->define(ShopTable::class, function (Faker $faker) {
    return [
        // faker is library to generate many data and use faker properties to generate for each variable
        'shop_name'=>$faker->name,
        'shop_address'=> $faker->text,
        'shop_mobile'=> rand(1000000000,2000000000),
        'shop_phno'=> rand(1000000000,2000000000),
        'shop_email'=> $faker->unique()->safeEmail,
        'shop_status'=> 1,

    ];
});
