

<script src="<?php echo e(asset('assets/js/custom.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/settings.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/gleek.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/styleSwitcher.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/prasudValidation.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/jquery.blockUI.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/pace/pace.min.js')); ?>"></script>

<!-- Morrisjs -->
<script src="<?php echo e(asset('assets/plugins/raphael/raphael.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/morris/morris.min.js')); ?>"></script>
<!-- Pignose Calender -->
<script src="<?php echo e(asset('assets/plugins/moment/moment.min.js')); ?>"></script>

<script src="<?php echo e(asset('assets/plugins/tables/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/tables/js/datatable/dataTables.bootstrap4.min.js')); ?>"></script>

<script src="<?php echo e(asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/clockpicker/dist/jquery-clockpicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>

<script src="<?php echo e(asset('assets/plugins/tables/js/datatable-init/datatable-basic.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset ('assets/plugins/sweetalert/js/sweetalert.min.js')); ?>"></script>

<script src="<?php echo e(asset ('assets//js/plugins-init/form-pickers-init.js')); ?>"></script>

<!-- <script src="./js/plugins-init/form-pickers-init.js"></script> -->
<?php /**PATH /var/www/laravel6crud/resources/views/layoutsections/scripts.blade.php ENDPATH**/ ?>