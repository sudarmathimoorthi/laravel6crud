<?php $__env->startSection('content'); ?>
<?php $role = session('role');?>
<div class="main-panel">
    <div class="content">
        <div class="container-fluid">    
              <div class="card ">
                  <div class="card-header mt-2">
                    <div class="row">
                        <div class="col-xl-10 col-lg-12">
                            <h3><a href="/employee" >User Details </a></h3>
                        </div>
                        <div class="col-xl-2 col-lg-12">
                            <a href="/employee/add" class="btn gradient-3 pull-right"><b>Add User</b></a>
                        </div>
                    </div>
                  </div>
                  <div class="card-body">
                      <?php if(session('status')): ?>
                          <div class="alert alert-success alert-dismissible fade show ml-5 mr-5 mt-1" role="alert" id="show_alert_index" ><div class="text-center" style="font-size: 18px;"><b>
                              <?php echo e(session('status')); ?></b></div>
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                          </div>
                          <script>$('#show_alert_index').delay(3000).fadeOut();</script>
                      <?php endif; ?>
                      <div class="table-responsive">
                        <table class="table table-striped table-bordered zero-configuration" id="employeeList">
                            <thead>
                                <tr>
                                    <th>User Name</th>
                                    <th>Mobile No</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <?php if($role == 'admin'): ?>
                                        <th>Action</th>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                  </div>
              </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    let userType = '<?php echo session("userType");?>';
  $(document).ready(function() {
    $.blockUI();
       
            displayAllEmployee()
  });

  function displayAllEmployee()
  {
    $.blockUI();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#employeeList').DataTable({
          processing: true,
          destroy : true,
          serverSide: true,
          scrollX: false,
          autoWidth:false,
          ajax: {
              url:'<?php echo e(url("displayAllEmployee")); ?>',
              type: 'POST',
          },
          columns: [
                  
                  { data: 'employee_name', name: 'employee_name',className:'firstcaps' },
                  { data: 'employee_phno', name: 'employee_phno' },
                  { data: 'employee_email', name: 'employee_email' },
                  { data: 'employee_status', name: 'employee_status' },
                  <?php if($role == 'admin') {?>
                  {data: 'action', name: 'action', orderable: false},
                  <?php } ?>
              ],
          order: [[0, 'desc']]
      });
      $.unblockUI();
  }
  
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/laravel6crud/resources/views/employee/index.blade.php ENDPATH**/ ?>