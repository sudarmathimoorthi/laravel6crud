<?php $__env->startSection('content'); ?>

<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">

            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item active"><a href="/admin">Admin</a></li>
            <li class="breadcrumb-item">Add</li>
        </ol>
    </div>
</div>
<!-- row -->

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                
                <div class="card-header mt-2">
                    <h3>Add Admin Details</h3>
                </div>
                <div class="alert alert-danger alert-dismissible fade show ml-5 mr-5 mt-2" id="showAlertdiv" role="alert" style="display:none"><span id="showAlertIndex"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="card-body">
                <div id="show_alert"  class="mt-4" style=""></div>
                <?php if($errors->any()): ?>
                    <div class="alert alert-danger alert-dismissible fade show">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <ul>
                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><?php echo e($error); ?></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                <?php endif; ?>
                    <div class="form-validation">
                        <form id="addAdmin" name="addAdmin"  enctype="multipart/form-data" action="/admin/add" method="post" onsubmit="validateNow();return false;">
                        <?php echo csrf_field(); ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="adminName">Admin Name <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" onkeypress="return isChar(event);" class="form-control col-md-11 isRequired" id="adminName" name="adminName" title="Please Enter Admin Name" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="adminPhNo">Admin Mobile <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" onkeypress="return isNumberKey(event);" class="form-control col-md-11 isRequired" id="adminPhNo" maxlength="10"  name="adminPhNo" title="Please Enter Admin Phone Number" autocomplete="off" onblur="checkNameValidation('admin','admin_mobile', this.id,'', 'Entered admin mobile no is already exist.')">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="password">Password <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" class="form-control col-md-11 isRequired" id="password" name="password" title="Please Enter Password" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="password">Confirm Password <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" class="form-control col-md-11 confirmPassword" name="password" title="Please Confirm Your Password" autocomplete="off" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="" for="email">Admin Email
                                                </label>
                                                <input type="text" class="form-control col-md-11 isEmail" id="email"  name="email" title="Please Enter Admin Valid Email" autocomplete="off" onblur="checkNameValidation('admin','admin_email', this.id,'', 'Entered admin email is already exist.')">
                                            </div>
                                        </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="adminStatus">Admin Status <span class="text-danger">*</span>
                                        </label>
                                        <select class="form-control col-md-11 isRequired" autocomplete="off" style="width:100%;" id="adminStatus" name="adminStatus" title="Please select status">
                                            <option value="1" selected>Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="address">Address<span class="text-danger">*</span>
                                        </label>
                                        <textarea class="col-md-11 isRequired" id="address" name="address" title="Please Enter Address" autocomplete="off"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" for="pincode">Pincode <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" onkeypress="return isNumberKey(event);" class="form-control col-md-11 isRequired" id="pincode" maxlength="6"  name="pincode" title="Please Enter Pincode" autocomplete="off" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="m-t-40">Date Timepicker</label>
                                    <input type="text" id="date-format" class="form-control" name="dateTime">
                                </div>
                                <div class="col-md-6">
                                    File Upload<span class="text-danger">*</span>
                                    <div class="custom-file mt-2">
                                        <input type="file" class="custom-file-input" id="file" name="file">
                                        <label class="custom-file-label" for="file">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mt-5">
                                <label class="col-lg-4 col-form-label"><a href="#">Required Fields</a>  <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" id="submitBtn" class="btn gradient-3 float-right ml-3" >Submit</button>
                                    <a href="/employee" class="btn btn-primary float-right">Cancel</a>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label"><a href="#">Required Fields</a>  <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                    <button type="submit" class="btn gradient-3 float-right ml-3">Submit</button>
                                    <a href="/admin" class="btn btn-primary float-right">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #/ container -->

<script>

$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

duplicateName = true;
    function validateNow() {
        $.blockUI();
        mobNum = $('#adminPhNo').val();
		if(mobNum.length!=10){
            $.unblockUI();
			$("html, body").animate({ scrollTop: 0 }, "slow");
            alert = '<div class="text-center" style="font-size: 18px;"><b>Please give 10  digit mobile number</b></div>'
			$("#showAlertIndex").html(alert);
			$('#showAlertdiv').show();
			ismob = false;
			$('#adminPhNo').css('background-color', 'rgb(255, 255, 153)')
			$('#adminPhNo').focus();
			$('#showAlertdiv').delay(3000).fadeOut();
		}
		else{
			ismob = true;
		}
        if(ismob==true){
            flag = validator.init({
                formId: 'addAdmin'
            });
            console.log(flag);
            if (flag == true) {
                if (duplicateName) {
                    document.getElementById('addAdmin').submit();
                }
            }
            else{
                $.unblockUI();
                // Swal.fire('Any fool can use a computer');
                $('#show_alert').html(flag).delay(3000).fadeOut();
                $('#show_alert').css("display","block");
            }
        }
    }

    function checkNameValidation(tableName, fieldName, obj, fnct, msg) {
		checkValue = document.getElementById(obj).value;
		if ($.trim(checkValue) != '') {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url: "<?php echo e(url('/checkNameValidation')); ?>",
				method: 'post',
				data: {
					tableName: tableName,
					fieldName: fieldName,
					value: checkValue,
					fnct: fnct,
				},
				success: function(result) {
					if (result > 0) {
                        $("html, body").animate({ scrollTop: 0 }, "slow");
						$("#showAlertIndex").text(msg);
						$('#showAlertdiv').show();
						duplicateName = false;
						document.getElementById(obj).value = "";
						$('#' + obj).focus();
						$('#' + obj).css('background-color', 'rgb(255, 255, 153)')
						$('#showAlertdiv').delay(3000).fadeOut();
					} else {
						duplicateName = true;
					}
				}
			});
		}
	}

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/laravel6crud/resources/views/admin/add.blade.php ENDPATH**/ ?>