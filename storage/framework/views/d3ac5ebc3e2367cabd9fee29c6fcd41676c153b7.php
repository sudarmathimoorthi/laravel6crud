<!--**********************************
    Nav header start
***********************************-->
<?php
use Illuminate\Support\Facades\Request;
$role = session('role');

?>
<div class="nav-header">
    <div class="brand-logo">
        <a href="/">
            <b class="logo-abbr" style="color:#fff;font-size: 20px;!important;">ZT</b>
            <span class="logo-compact"></span>
            <span class="brand-title" style="color:#fff;font-size: 20px;!important;">
                Zaigo Task
                <!-- <img src="<?php echo e(asset('assets/images/logo-text.png')); ?>" alt=""> -->
                <!-- <img class="brand-logo" alt="modern admin logo" src="http://jumbo.deforay.in/assets/images/logo/JBL-logo.JPG"> -->
            </span>
        </a>
    </div>
</div>
<!--**********************************
    Nav header end
***********************************-->

<!--**********************************
    Header start
***********************************-->
<div class="header">    
    <div class="header-content clearfix">
        
        <div class="nav-control">
            <div class="hamburger">
                <span class="toggle-icon"><i class="icon-menu"></i></span>
            </div>
        </div>
        <div class="header-left">

        </div>
        <div class="header-right">
            <ul class="clearfix">
                <li class="icons">
                            <?php if(session('name')): ?>
                        <span class="user-name text-bold-700">Welcome <?php echo e(ucfirst(session('name'))); ?></span>
                        <?php endif; ?>
                    </a>
                    
                </li>
                <li class="icons dropdown">
                    <div class="user-img c-pointer position-relative" data-toggle="dropdown">
                        <span class="activity active"></span>
                        <?php if(session('userImg')): ?>
                        <img src="<?php echo e(asset('../'.session('userImg'))); ?>" height="40" width="40" alt="">
                        <?php else: ?>
                        <img src="<?php echo e(asset('assets/images/avatar/3.png')); ?>" height="40" width="40" alt="">
                        <?php endif; ?>
                    </div>
                    <div class="drop-down dropdown-profile animated fadeIn dropdown-menu">
                        <div class="dropdown-content-body">
                            <ul>
                                <?php  if(session('userType') == 'admin') { ?>
                                <?php if (isset($role['App\\Http\\Controllers\\Admin\\AdminController']['edit']) && ($role['App\\Http\\Controllers\\Admin\\AdminController']['edit'] == 'allow')){ ?>
                                    <li>
                                    <a href="/admin/edit/<?php echo e(base64_encode(session('userId'))); ?>"><i class="icon-user"></i> <span>Profile</span></a>
                                </li>
                                <hr class="my-2">
                                <?php } ?>
                                    <?php if (isset($role['App\\Http\\Controllers\\Admin\\AdminController']['changePassword']) && ($role['App\\Http\\Controllers\\Admin\\AdminController']['changePassword'] == 'allow')){ ?>
                                    <li>
                                <a href="/admin/changePassword/<?php echo e(base64_encode(session('userId'))); ?> "><i class="icon-user"></i> Change Password</a>  
                                </li>
                                <hr class="my-2">
                                <?php } ?>
                                <form action="/adminlogout" name="adminlogoutForm" id="adminlogoutForm" method="POST">
                                    <?php echo csrf_field(); ?>
                                    <button type="submit" class="dropdown-item"><i class="icon-key"></i> Logout</button>
                                </form>
                                <?php }else {?>
                                    <?php if (isset($role['App\\Http\\Controllers\\Employee\\EmployeeController']['edit']) && ($role['App\\Http\\Controllers\\Employee\\EmployeeController']['edit'] == 'allow')){ ?>
                                    <li>
                                    <a href="/employee/edit/<?php echo e(base64_encode(session('userId'))); ?>"><i class="icon-user"></i> <span>Profile</span></a>
                                </li>
                                <hr class="my-2">
                                <?php } ?>
                                    <?php if (isset($role['App\\Http\\Controllers\\Employee\\EmployeeController']['changePassword']) && ($role['App\\Http\\Controllers\\Employee\\EmployeeController']['changePassword'] == 'allow')){ ?>
                                    <li>
                                <a href="/employee/changePassword/<?php echo e(base64_encode(session('userId'))); ?> "><i class="icon-user"></i> Change Password</a>  
                                </li>
                                <hr class="my-2">
                                    <?php } ?>
                                <form action="/logout" name="logoutForm" id="logoutForm" method="POST">
                                    <?php echo csrf_field(); ?>
                                    <button type="submit" class="dropdown-item"><i class="icon-key"></i> Logout</button>
                                </form>
                                <?php }?>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--**********************************
    Header end ti-comment-alt
***********************************--><?php /**PATH /var/www/laravel6crud/resources/views/layoutsections/navheader.blade.php ENDPATH**/ ?>