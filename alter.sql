ALTER TABLE `employee` ADD `date_time` DATETIME NULL AFTER `employee_img`;
ALTER TABLE `employee` ADD `address` TEXT NULL AFTER `date_time`, ADD `pincode` VARCHAR(50) NULL AFTER `address`;
ALTER TABLE `employee` CHANGE `date_time` `date_time` VARCHAR(255) NULL DEFAULT NULL;
ALTER TABLE `admin` ADD `address` TEXT NULL AFTER `admin_status`, ADD `pincode` VARCHAR(50) NULL AFTER `address`, ADD `date_time` VARCHAR(255) NULL AFTER `pincode`;
ALTER TABLE `admin` ADD `admin_file` VARCHAR(255) NULL AFTER `date_time`;
